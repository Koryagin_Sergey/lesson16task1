package sourceit.com.lesson16task1;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Generator {

    private Generator() {
    }

    public static Contact[] generate() {
        Contact[] contacts = {
                new Contact("Name1", "email1", "City1", R.drawable.icon_phone),
                new Contact("Name2", "email2", "City2", R.drawable.icon_phone),
                new Contact("Name3", "email3", "City3", R.drawable.icon_phone),
                new Contact("Name4", "email4", "City4", R.drawable.icon_phone),
                new Contact("Name5", "email5", "City5", R.drawable.icon_phone),
        };

        return contacts;
    }

    public static List<Contact> generateList() {
        return new ArrayList<>(Arrays.asList(generate()));
    }
}
