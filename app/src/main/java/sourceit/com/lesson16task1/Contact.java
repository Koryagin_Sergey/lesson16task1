package sourceit.com.lesson16task1;


public class Contact {

    private String name;
    private String email;
    private String address;
    private int image;

    public Contact(String name, String email, String address, int image) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public int getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setImage(int image) {
        this.image = image;
    }

}
